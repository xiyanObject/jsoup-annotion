### 介绍
原始的Jsoup爬虫框架使用起来可能比较繁琐，特别是在进行封装对象、级联封装对象(表中表)的时候，常见的做法是针对每个对象编写解决方法。

该框架的作用是减少公共代码，简化操作，不重复造轮子，只需要在类和字段上添加一个的注解，就可以把爬取的内容封装成一个对象，并提供常用的功能，可以基本满足日常的需求。

框架功能：

1、支持爬取文本、HTML、任意标签的属性，并提供分页爬取方法。

2、提供完全匹配、包含、字符串替换、>、<、=等筛选功能。

3、提供自动封装对象、自动封装List集合、级联爬取(表中表)功能

4、提供爬取图片保存到指定目录功能，同时可以自定义图片格式。

### 涉及技术
反射、IO、Jsoup、自定义注解、DCL单例

### 安装教程

**将这个jar包拷贝到你的项目中即可** 
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/233232_96061dd8_4967101.png)

### 使用说明

更加完整的教程请转到：https://blog.csdn.net/qq_43332570/article/details/107920692

详细api见下个title、

#### 1、爬取单一对象
> 步骤：

a、在实体类对象和字段上添加注解
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_6fc4cd84_4967101.png)
b、执行
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_689a7412_4967101.png)
结果如下：
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_74bc9e7b_4967101.png)
#### 2、爬取List集合

> 步骤：

a、在实体类对象和字段上添加注解(跟上面相同)
b、执行（注意传递的是List）
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_f567f6c1_4967101.png)
结果如下：
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_ff21abb2_4967101.png)
#### 3、爬取并保存图片
以爬取jd商城的商品列表为例

> 步骤

a、在实体类对象和字段上添加注解
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_ee1df992_4967101.png)
b、执行
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_7d4ed35d_4967101.png)
结果：
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_651e4ceb_4967101.png)
![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0810/215525_d40b3b29_4967101.png)

**PS：完整Demo请下载该项目进行查看。**

### 详细Api说明
**@JsoupDocment：** 标注在实体类或字段上
 - 若标注在类上：表示该类为爬虫填充实体对象
 - 标注在字段上：表示该字段为另外一个实体对象

|  属性 |  说明 |
|---|---|
|  targetUrl |  爬取网站的url  |
| domain |  目标网站的首页，有时候需要拼接url，若targetUrl不存在则以这个为准 |
| cssQuery | css选择器，注意是一个封装实体的css选择器，不加则表示爬取整个页面  |
| subHrefByCss | 仅级联爬取时使用，会从该选择器的href属性指定的url去爬取，一般为a标签  |
| subHrefByField | 仅级联爬取时使用，从类中的字段上获取url去爬取  |
| urlPrefix | 仅级联爬取时使用，有时候级联爬取网站时可能是相对路径，可以通过该属性执行前缀，默认拼接domain属性的值，注意：若是以http开头url则不会添加该前缀  |

**@JsoupField：** 只能标注在字段上，

`注意：该字段的类所在必须含有@JsoupDocment注解`

|  属性 |  说明 |
|---|---|
| cssQuery  |  将指定选择器的中值注入到这个字段上,注意：这是基于JsoupDocment注解指定的css选择器的 |
|  textType |  是获取text还是html，ExtractTextType枚举值，默认JsoupTextType.TEXT |
|  attr |  指定从属性上爬取，如src，若指定该值则textType属性将不起作用 |
| replaceTarget  | 字符串替换功能，要替换的目标字符串，默认替换为空字符串，可通过replaceRes指定替换为指定字符串  |
|  replaceRes | 要替换成的目标字符  |
|  isInteger | 字符是否需要转换为Integer类型，默认false  |
|  imgSaveAttr |  仅在保存图片时使用，指定从img标签的哪个属性上爬取图片，注意：若要保存图片，该属性必须指定 |
|  imgSavePath | 图片保存位置，imgAttr属性存在时生效，默认保存在user.dir(当前项目路径)  |
| imgFormat  |  保存文本时图片格式，ImgType枚举值，默认为ImgType.PNG|
| filterStrByEquals  |  字符串筛选时使用，完全匹配 |
|  filterStrByContain | 字符串筛选时使用，是否包含指定字符串，当filterByEquals也存在时以filterByContain为准  |
|  filterIntByEq | 筛选数字，=，仅当isInteger为true时生效，判断顺序：大于 、小于、等于  |
|  filterIntByGt | 筛选数字，<，仅当isInteger为true时生效，判断顺序：大于 、小于、等于|
|  filterIntByLt |  筛选数字，<，仅当isInteger为true时生效，判断顺序：大于 、小于、等于 |

**execute方法：**
    核心方法，用来执行该框架并获取结果

|  方法 |  说明 |
|---|---|
|  public Object execute(Object obj,Class<?> clazz) | obj：返回值类型，clazz：实体的class  |
| public Object execute(Object obj,Class<?> clazz,String url) | url：爬取的url，忽略@JsoupDocment注解指定的url ，在分页爬取时可使用|

