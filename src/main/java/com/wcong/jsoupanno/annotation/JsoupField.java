package com.wcong.jsoupanno.annotation;


import com.wcong.jsoupanno.states.ExtractTextType;
import com.wcong.jsoupanno.states.ImgType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 只能标注在字段上
 * 注意：该字段的类所在必须含有@JsoupDocment注解
 * @author wcong
 * @version 1.0
 * @date 2020-08-06 22:17
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JsoupField {

    /**
     * 将指定选择器的中值注入到这个字段上
     * 注意：这是基于JsoupDocment注解指定的css选择器的
     * @return
     */
    String cssQuery();

    /**
     * 是获取text还是html，ExtractTextType枚举值
     * 默认JsoupTextType.TEXT
     * @return
     */
    ExtractTextType textType() default ExtractTextType.TEXT;

    /**
     * 指定从属性上爬取，如src
     * 若指定该值则textType属性将不起作用
     * @return
     */
    String attr() default "";

    /**
     * 字符串替换功能，要替换的目标字符串
     * 默认替换为空字符串
     * 可通过replaceRes指定替换为指定字符串
     * @return
     */
    String replaceTarget() default "";

    /**
     * 要替换成的目标字符
     * @return
     */
    String replaceRes() default "";

    /**
     * 字符是否需要转换为Integer类型
     * 默认不转换
     * @return
     */
    boolean isInteger() default false;

    /**
     * 仅在保存图片时使用
     * 指定从img标签的哪个属性上爬取图片
     * 注意：若要保存图片，该属性必须指定
     * @return
     */
    String imgSaveAttr() default "";

    /**
     * 图片保存位置，imgAttr属性存在时生效
     * 默认保存在user.dir(当前项目路径)
     * @return
     */
    String imgSavePath() default "";

    /**
     * 保存文本时图片格式
     * 默认为png
     * @return
     */
    ImgType imgFormat() default ImgType.PNG;

    /**
     * 字符串筛选时使用
     * 完全匹配
     * @return
     */
    String filterStrByEquals() default "";

    /**
     * 字符串筛选时使用
     * 是否包含指定字符串
     * 当filterByEquals也存在时以filterByContain为准
     * @return
     */
    String filterStrByContain() default "";

    /**
     * 筛选数字，=
     * 仅当isInteger为true时生效
     * 判断顺序：大于 、小于、等于
     * @return
     */
    int filterIntByEq() default -1;

    /**
     * 筛选数字，>
     * 仅当isInteger为true时生效
     * 判断顺序：大于 、小于、等于
     * @return
     */
    int filterIntByGt() default -1;

    /**
     * 筛选数字，<
     * 仅当isInteger为true时生效
     * 判断顺序：大于 、小于、等于
     * @return
     */
    int filterIntByLt() default -1;

}
