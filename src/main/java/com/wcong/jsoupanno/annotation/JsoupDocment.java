package com.wcong.jsoupanno.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标注在实体类或字段上
 * 若标注在类上：表示该类为爬虫填充实体对象
 * 标注在字段上：表示该字段为另外一个实体对象
 * @author wcong
 * @version 1.0
 * @date 2020-08-03 21:02
 */
@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface JsoupDocment {

    /**
     * 爬取目标网站的url
     * @return
     */
    String targetUrl() default "";

    /**
     * 爬取网站的首页
     * 若targetUrl不存在则以这个为准
     *
     * @return
     */
    String domain();

    /**
     * css选择器
     * 注意是一个实体的css选择器
     * 不加则表示爬取整个页面
     * @return
     */
    String cssQuery() default "";

    /**
     * 仅级联爬取时使用
     * 会从该选择器的href属性指定的url去爬取
     * 一般为a标签
     *
     * @return
     */
    String subHrefByCss() default "";


    /**
     * 仅级联爬取时使用
     * 从类中的字段上获取url
     * 当subHrefCss存在时，以subHrefCss为准
     * 若两个属性都不存在则获取第一个a标签的href属性
     * @return
     */
    String subHrefByField() default "";

    /**
     * 仅级联爬取时使用
     * 有时候级联爬取网站时可能是相对路径
     * 可以通过该属性执行前缀，默认拼接domain属性的值
     * 注意：若是以http开头url则不会添加该前缀
     * @return
     */
    String urlPrefix() default "";

}
