package com.wcong.jsoupanno.states;

/**
 * 爬取图片类型
 * @author wcong
 * @version 1.0
 * @date 2020-08-06 22:57
 */
public enum ImgType {
    /**
     * png
     */
    PNG(1,".png"),
    /**
     * jpg
     */
    JPG(2,".jpg"),
    /**
     * gif
     */
    GIF(3,".gif")
    ;

    ImgType(int textType, String textName){
        this.textType = textType;
        this.textName = textName;
    }


    private int textType;
    private String textName;

    public int getTextType() {
        return textType;
    }

    public String getTextName() {
        return textName;
    }
}
