package com.wcong.jsoupanno.states;

/**
 * 爬取文本类型状态枚举
 * @author wcong
 * @version 1.0
 * @date 2020-08-06 22:57
 */
public enum ExtractTextType {
    /**
     * 爬取text，不包括HTML
     */
    TEXT(1,"TEXT"),
    /**
     * 爬取HTML
     */
    HTML(2,"HTML")
    ;

    ExtractTextType(int textType,String textName){
        this.textType = textType;
        this.textName = textName;
    }


    private int textType;
    private String textName;

    public int getTextType() {
        return textType;
    }

    public String getTextName() {
        return textName;
    }
}
