package com.wcong.jsoupanno.pojo;

import com.wcong.jsoupanno.annotation.JsoupDocment;
import com.wcong.jsoupanno.annotation.JsoupField;

/**
 * 京东商品实体
 * @author wcong
 * @version 1.0
 * @date 2020-08-09 13:39
 */
@JsoupDocment(domain = "https://search.jd.com",
        targetUrl = "https://search.jd.com/Search?keyword=java&enc=utf-8&wq=java&pvid=bc21d42a61d44dda8f5f8b743b85b53d",
        cssQuery = ".gl-item")
public class JDPojo {

    /**
     * 商品图片
     */
    @JsoupField(cssQuery = ".p-img img",attr = "src",imgSaveAttr = "src",imgSavePath = "E:/tmp/img")
    private String shopImg;

    /**
     * 商品价格
     */
    @JsoupField(cssQuery = ".p-price")
    private String shopPrice;

    /**
     * 商品名称
     */
    @JsoupField(cssQuery = ".p-name")
    private String shopTitle;

    /**
     * 店铺名称
     */
    @JsoupField(cssQuery = ".p-shop")
    private String shopName;

    public String getShopImg() {
        return shopImg;
    }

    public void setShopImg(String shopImg) {
        this.shopImg = shopImg;
    }

    public String getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(String shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("JDPojo{");
        sb.append("shopImg='").append(shopImg).append('\'');
        sb.append(", shopPrice='").append(shopPrice).append('\'');
        sb.append(", shopTitle='").append(shopTitle).append('\'');
        sb.append(", shopName='").append(shopName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
