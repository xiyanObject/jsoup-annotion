package com.wcong.jsoupanno.pojo;

import com.wcong.jsoupanno.annotation.JsoupDocment;
import com.wcong.jsoupanno.annotation.JsoupField;

/**
 * 博客实体
 * @author wcong
 * @version 1.0
 * @date 2020-08-06 23:16
 */
@JsoupDocment(targetUrl = "https://blog.csdn.net/qq_43332570",
        cssQuery = ".article-item-box",
        domain = "https://blog.csdn.net")
public class BlogDoc {

    /**
     * 博客标题
     */
    @JsoupField(cssQuery = "h4",
            replaceTarget = "原创 ")
    private String title;

    /**
     * 博客描述
     */
    @JsoupField(cssQuery = ".content")
    private String remark;

    /**
     * 发布时间
     */
    @JsoupField(cssQuery = ".date")
    private String time;

    /**
     * 阅读数
     */
    @JsoupField(cssQuery = ".read-num",isInteger = true)
    private Integer readCount;

    /**
     * 博客链接
     */
    @JsoupField(cssQuery = "h4 > a",attr = "href")
    private String blogLink;

    /**
     * 博客内容：级联爬取
     */
//    @JsoupDocment(domain = "https://blog.csdn.net",subHrefByCss = "h4 > a")
    private BlogContent blogContent;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    public BlogContent getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(BlogContent blogContent) {
        this.blogContent = blogContent;
    }

    public String getBlogLink() {
        return blogLink;
    }

    public void setBlogLink(String blogLink) {
        this.blogLink = blogLink;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BlogDoc{");
        sb.append("title='").append(title).append('\'');
        sb.append(", remark='").append(remark).append('\'');
        sb.append(", time='").append(time).append('\'');
        sb.append(", readCount=").append(readCount);
        sb.append(", blogContent=").append(blogContent);
        sb.append(", blogLink='").append(blogLink).append('\'');
        sb.append('}');
        return sb.toString();
    }
}



