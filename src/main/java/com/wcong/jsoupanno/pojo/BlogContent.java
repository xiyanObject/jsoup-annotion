package com.wcong.jsoupanno.pojo;


import com.wcong.jsoupanno.annotation.JsoupDocment;
import com.wcong.jsoupanno.annotation.JsoupField;
import com.wcong.jsoupanno.states.ExtractTextType;

/**
 * 博客内容实体
 * @author wcong
 * @version 1.0
 * @date 2020-08-07 23:43
 */
@JsoupDocment(domain = "https://blog.csdn.net")
public class BlogContent {

    /**
     * 标题
     */
    @JsoupField(cssQuery = ".title-article")
    private String blogTitle;

    /**
     * 作者
     */
    @JsoupField(cssQuery = ".follow-nickName")
    private String blogOther;

    /**
     * 分类
     */
    @JsoupField(cssQuery = ".bar-content > .time")
    private String blogType;

    /**
     * 博客内容，HTML
     */
    @JsoupField(cssQuery = "#content_views",textType = ExtractTextType.HTML)
    private String blogText;

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public String getBlogOther() {
        return blogOther;
    }

    public void setBlogOther(String blogOther) {
        this.blogOther = blogOther;
    }

    public String getBlogType() {
        return blogType;
    }

    public void setBlogType(String blogType) {
        this.blogType = blogType;
    }

    public String getBlogText() {
        return blogText;
    }

    public void setBlogText(String blogText) {
        this.blogText = blogText;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BlogContent{");
        sb.append("blogTitle='").append(blogTitle).append('\'');
        sb.append(", blogOther='").append(blogOther).append('\'');
        sb.append(", blogType='").append(blogType).append('\'');
        sb.append(", blogText='").append(blogText).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
