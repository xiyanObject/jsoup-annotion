package com.wcong.jsoupanno.test;


import com.wcong.jsoupanno.handler.JsoupHandler;
import com.wcong.jsoupanno.pojo.BlogDoc;
import com.wcong.jsoupanno.pojo.BlogDocFilter;
import com.wcong.jsoupanno.pojo.JDPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wcong
 * @version 1.0
 * @date 2020-08-07 0:01
 */
public class Test {
    public static void main(String[] args) throws Exception {
//        testGetBlog();
//        testGetBlogList();
            testGetBlogFilterList();
//        testGetShop();
//        testGetShopList();
    }

    /**
     * 测试爬取博客列表
     * @throws Exception
     */
    private static void testGetBlogList() throws Exception {
        List<BlogDoc> blogDocs = (List<BlogDoc>) JsoupHandler.getInstance().execute(new ArrayList<BlogDoc>(),BlogDoc.class);
        blogDocs.forEach(System.out::println);
    }

    /**
     * 测试爬取博客列表+过滤
     * @throws Exception
     */
    private static void testGetBlogFilterList() throws Exception {
        List<BlogDocFilter> blogDocs = (List<BlogDocFilter>) JsoupHandler.getInstance().execute(new ArrayList<BlogDocFilter>(),BlogDocFilter.class);
        blogDocs.forEach(System.out::println);
    }

    /**
     * 测试爬取单一博客列表
     * @throws Exception
     */
    public static void testGetBlog() throws Exception {
        BlogDoc execute = (BlogDoc) JsoupHandler.getInstance().execute(new BlogDoc(),BlogDoc.class);
        System.out.println(execute);
    }

    /**
     * 测试爬取单一的jd商品 + 保存图片
     * @throws Exception
     */
    public static void testGetShop() throws Exception {
        JDPojo jdPojo = (JDPojo) JsoupHandler.getInstance().execute(new JDPojo(),JDPojo.class);
        System.out.println(jdPojo);
    }

    /**
     * 测试爬取京东商品列表 + 保存图片
     * @throws Exception
     */
    public static void testGetShopList() throws Exception {
        List<JDPojo> jdPojos = (List<JDPojo>) JsoupHandler.getInstance().execute(new ArrayList<JDPojo>(),JDPojo.class);
        jdPojos.forEach(System.out::println);
    }

}
